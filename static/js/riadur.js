var riadur = false;

function search(){
    var do_search = function () {
        var lang = escape($('[name=lang]:checked').val());
        var word = escape($('#search').val());
        $('#block')
        .html('<div style="margin:0 auto 0;width:48px;"><img src="static/images/loader.gif"/></div>')
        .load('/dictionary/' + lang + '/' + word, function () {
            location.hash = '#' + lang + ';' + word;
        });
        return false;
    };
    if (riadur) {
        do_search();
        return false;
    }
    $('#header').animate({
        marginLeft:(($(window).width()-600)/2)+"px"
    },1500, function(){
        $('#header img').animate({
            width:'130px',
            height:'130px'
        },1500);
        $('#search_box').animate({
            marginTop:'-120px',
            marginLeft:($(window).width()/2)+"px"
        },1500,function(){
            riadur = true;
            do_search();
        });
    });
    return false;
}

$(function(){
    $("#search").placeholdr();
    $('form').submit(search);
    if(location.hash!==''){
        query = location.hash.replace(/#/,"").split(";");
        lang = query.shift();
        $("[name=lang]").filter("[value="+lang+"]").attr("checked","checked");
        $("[name=search]").val(query.join(" "));
        search();
    }
});
